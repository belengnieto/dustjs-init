var path = require('path');
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-dustjs');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.initConfig({
      helpers:          '<%=paths.build%>/dust-helpers.js',
      helpersMin:       '<%=paths.build%>/dust-helpers.min.js',
    dustjs: {
      compile: {
        files: {
           'scripts/src/plp.js': ['templates/*.dust']
          },
      }
    },

    less: {
      dist: {
        files: {
          'css/styles.css': 'bem/styles.less'
        }
      }
    },

    watch: {
      templates: {
        files: ['templates/*'],
        tasks: ['dustjs']
      },

      styles: {
        files: ['bem/**/*.less','bem/_partials/**/*.less'],
        tasks: ['less:dist'],
        options: {
         livereload: true,
         spawn: false
      }
      }


    }
  });
  grunt.registerTask('default', ['less:dist','watch']);  
}


