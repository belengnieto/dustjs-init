var listados = 	{ 
  "listado_ofertas": [
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914142800402": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914142800402": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120902700135___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "14,90 &euro;",
            "onsale": true,
            "salePriceFormatted": "14,35 &euro;",
            "PUMPriceFormatted": "2,87 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/13/5/0120902700135/0120902700135000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/13/5/0120902700135/0120902700135000p01011.jpg"
          },
          "description": "COOSUR aceite de oliva suave 0,4º bidon 5 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914142800401": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914142800401": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120902600020___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "2,99 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/0/0120902600020/0120902600020000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/0/0120902600020/0120902600020000p01011.jpg"
          },
          "description": "COOSUR aceite de oliva suave 0,4º botella 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914142800402": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914142800402": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120903100020___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "14,90 &euro;",
            "onsale": true,
            "salePriceFormatted": "14,35 &euro;",
            "PUMPriceFormatted": "2,87 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/0/0120903100020/0120903100020000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/0/0120903100020/0120903100020000p01011.jpg"
          },
          "description": "COOSUR aceite de oliva 1º intenso bidon 5 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914142800401": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914142800401": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120903000022___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,26 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,99 &euro;",
            "PUMPriceFormatted": "2,99 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/2/0120903000022/0120903000022000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/2/0120903000022/0120903000022000p01011.jpg"
          },
          "description": "COOSUR aceite de oliva intenso 1º botella 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceites y vinagres > Aceite de oliva"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904000260___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva virgen",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "17,45 &euro;",
            "onsale": true,
            "salePriceFormatted": "12,50 &euro;",
            "PUMPriceFormatted": "2,50 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/26/0/0120904000260/0120904000260000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/26/0/0120904000260/0120904000260000p01011.jpg"
          },
          "description": "EL CORTE INGLES aceite de oliva virgen extra Hojiblanca bidon 5 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120903900320___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva virgen",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,55 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,55 &euro;",
            "PUMPriceFormatted": "2,55 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/32/0/0120903900320/0120903900320000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/32/0/0120903900320/0120903900320000p01011.jpg"
          },
          "description": "EL CORTE INGLES aceite de oliva virgen extra Hojiblanca botella 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914142800501": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914142800501": ""
          },
          "productId": "0110120903700027___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceite de oliva virgen",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,24 &euro;",
            "PUMPriceFormatted": "3,24 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/02/7/0120903700027/0120903700027000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/02/7/0120903700027/0120903700027000p01011.jpg"
          },
          "description": "CARBONELL aceite de oliva virgen botella 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceites y vinagres > Aceite de oliva virgen"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400080___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,87 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,49 &euro;",
            "PUMPriceFormatted": "12,69 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/08/0/0120904400080/0120904400080000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/08/0/0120904400080/0120904400080000p01011.jpg"
          },
          "description": "CARBONELL crema de vinagre Pedro Ximénez envase 275 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400064___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,05 &euro;",
            "PUMPriceFormatted": "12,20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/4/0120904400064/0120904400064000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/4/0120904400064/0120904400064000p01011.jpg"
          },
          "description": "PONTI Glasa crema de vinagre balsámico de Módena bote 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400049___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,59 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,29 &euro;",
            "PUMPriceFormatted": "13,16 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/04/9/0120904400049/0120904400049000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/04/9/0120904400049/0120904400049000p01011.jpg"
          },
          "description": "BORGES crema de vinagre balsámico de Módena envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400056___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,58 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,29 &euro;",
            "PUMPriceFormatted": "13,16 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/6/0120904400056/0120904400056000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/6/0120904400056/0120904400056000p01011.jpg"
          },
          "description": "BORGES crema de vinagre balsámico de manzana envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400205___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,59 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,29 &euro;",
            "PUMPriceFormatted": "13,16 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/20/5/0120904400205/0120904400205000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/20/5/0120904400205/0120904400205000p01011.jpg"
          },
          "description": "BORGES crema balsámica de frambuesa envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400189___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,19 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,75 &euro;",
            "PUMPriceFormatted": "11,46 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/18/9/0120904400189/0120904400189000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/18/9/0120904400189/0120904400189000p01011.jpg"
          },
          "description": "EL GUISO reducción de vino Pedro Ximénez envase 250 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400197___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,97 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,55 &euro;",
            "PUMPriceFormatted": "10,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/19/7/0120904400197/0120904400197000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/19/7/0120904400197/0120904400197000p01011.jpg"
          },
          "description": "EL GUISO reducción de vinagre balsámico Pedro Ximénez botella 250 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400213___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,69 &euro;",
            "PUMPriceFormatted": "11,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/21/3/0120904400213/0120904400213000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/21/3/0120904400213/0120904400213000p01011.jpg"
          },
          "description": "SIBARI crema de vinagre balsámico a la frambuesa botella 225 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400254___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,69 &euro;",
            "PUMPriceFormatted": "11,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/25/4/0120904400254/0120904400254000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/25/4/0120904400254/0120904400254000p01011.jpg"
          },
          "description": "SIBARI crema de vinagre balsámico de chocolate botella 225 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400239___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,69 &euro;",
            "PUMPriceFormatted": "11,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/23/9/0120904400239/0120904400239000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/23/9/0120904400239/0120904400239000p01011.jpg"
          },
          "description": "SIBARI crema de vinagre balsámico de Modena botella 225 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400247___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vinagres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,69 &euro;",
            "PUMPriceFormatted": "11,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/24/7/0120904400247/0120904400247000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/24/7/0120904400247/0120904400247000p01011.jpg"
          },
          "description": "SIBARI crema de vinagre balsámico de vino blanco botella 225 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceites y vinagres > Vinagres"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800005": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800005": ""
          },
          "productId": "0110120941900241___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas aliñadas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,71 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,51 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/24/1/0120941900241/0120941900241000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/24/1/0120941900241/0120941900241000p01011.jpg"
          },
          "description": "AMANIDA ORO aceitunas verdes con hueso aliñadas con aceite de oliva frasco 390 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800001": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800001": ""
          },
          "productId": "0110120940401373___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas aliñadas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,67 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "11,47 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/37/3/0120940401373/0120940401373000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/37/3/0120940401373/0120940401373000p01011.jpg"
          },
          "description": "AMANIDA ORO aceitunas aliñadas verdes sin hueso frasco 320 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceitunas y variantes > Aceitunas aliñadas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800201": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120941700120___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas negras",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,32 &euro;",
            "PUMPriceFormatted": "7,14 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/12/0/0120941700120/0120941700120000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/12/0/0120941700120/0120941700120000p01011.jpg"
          },
          "description": "SERPIS aceitunas negras cacereñas con hueso lata 185 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800201": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120941700138___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas negras",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,32 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "8,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/13/8/0120941700138/0120941700138000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/13/8/0120941700138/0120941700138000p01011.jpg"
          },
          "description": "SERPIS aceitunas negras cacereñas sin hueso lata 150 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceitunas y variantes > Aceitunas negras"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914139800001": "Por la compra de 2 unidades regalo de una bolsa de patatas fritas El Corte Inglés de 170 g"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914139800001": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110120940800483___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,15 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,14 &euro;",
            "PUMPriceFormatted": "14,27 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/48/3/0120940800483/0120940800483000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/48/3/0120940800483/0120940800483000p01011.jpg"
          },
          "description": "EL CORTE INGLES aceitunas rellenas de anchoa pack 3 latas 50 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914139800001": "Por la compra de 2 unidades regalo de una bolsa de patatas fritas El Corte Inglés de 170 g"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914139800001": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110120940800491___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,04 &euro;",
            "PUMPriceFormatted": "13,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/49/1/0120940800491/0120940800491000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/49/1/0120940800491/0120940800491000p01011.jpg"
          },
          "description": "EL CORTE INGLES aceitunas rellenas de pimiento pack 3 latas 50 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120940800095___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,08 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,89 &euro;",
            "PUMPriceFormatted": "12,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/09/5/0120940800095/0120940800095000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/09/5/0120940800095/0120940800095000p01011.jpg"
          },
          "description": "LA ESPAÑOLA aceitunas suaves rellenas de anchoa pack 3 latas 50 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120940800210___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,09 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,89 &euro;",
            "PUMPriceFormatted": "12,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/21/0/0120940800210/0120940800210000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/21/0/0120940800210/0120940800210000p01011.jpg"
          },
          "description": "LA ESPAÑOLA aceitunas rellenas de anchoa pack 3 latas 50 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120940800236___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,11 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,89 &euro;",
            "PUMPriceFormatted": "12,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/23/6/0120940800236/0120940800236000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/23/6/0120940800236/0120940800236000p01011.jpg"
          },
          "description": "LA ESPAÑOLA TE CUIDA aceitunas rellenas de anchoa con omega 3 pack 3 latas 50 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800201": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120940800129___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,32 &euro;",
            "PUMPriceFormatted": "8,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/12/9/0120940800129/0120940800129000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/12/9/0120940800129/0120940800129000p01011.jpg"
          },
          "description": "SERPIS aceitunas rellenas de anchoa lata 150 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800201": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120940800160___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,49 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,32 &euro;",
            "PUMPriceFormatted": "8,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/16/0/0120940800160/0120940800160000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/16/0/0120940800160/0120940800160000p01011.jpg"
          },
          "description": "SERPIS aceitunas rellenas de anchoa ligeras lata 150 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800201": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120940800152___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,32 &euro;",
            "PUMPriceFormatted": "8,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/15/2/0120940800152/0120940800152000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/15/2/0120940800152/0120940800152000p01011.jpg"
          },
          "description": "SERPIS aceitunas rellenas de pimiento lata 150 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120940800582___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aceitunas rellenas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,16 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "6,67 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/58/2/0120940800582/0120940800582000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/58/2/0120940800582/0120940800582000p01011.jpg"
          },
          "description": "FRAGATA aceitunas rellenas de anchoa lata 150 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceitunas y variantes > Aceitunas rellenas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800008": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800008": ""
          },
          "productId": "0110120965200510___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aperitivos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,79 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,48 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/51/0/0120965200510/0120965200510000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/51/0/0120965200510/0120965200510000p01011.jpg"
          },
          "description": "AMANIDA ORO ajo al pesto frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800006": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800006": ""
          },
          "productId": "0110120965200197___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aperitivos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,48 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,41 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/19/7/0120965200197/0120965200197000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/19/7/0120965200197/0120965200197000p01011.jpg"
          },
          "description": "AMANIDA ORO Ajoliva con aceite de oliva frasco 370 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800007": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800007": ""
          },
          "productId": "0110120965200221___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aperitivos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,49 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,43 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/22/1/0120965200221/0120965200221000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/22/1/0120965200221/0120965200221000p01011.jpg"
          },
          "description": "AMANIDA ORO ajoblanco con aceite de oliva frasco 370 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceitunas y variantes > Aperitivos diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800401": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800401": ""
          },
          "productId": "0110120940900028___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pepinillos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,22 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "8,94 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/8/0120940900028/0120940900028000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/8/0120940900028/0120940900028000p01011.jpg"
          },
          "description": "KUHNE pepinillos frasco 360 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141125501": "Lleva 3 y paga 2"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141125501": ""
          },
          "productId": "0110120940900069___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pepinillos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,01 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "9,95 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/9/0120940900069/0120940900069000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/9/0120940900069/0120940900069000p01011.jpg"
          },
          "description": "EL CORTE INGLES pepinillos pequeños frasco 200 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aceitunas y variantes > Pepinillos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800901": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800901": ""
          },
          "productId": "0110120968400497___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Almendras",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,96 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "17,94 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/49/7/0120968400497/0120968400497000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/49/7/0120968400497/0120968400497000p01011.jpg"
          },
          "description": "MATUTANO almendras tostadas al horno bolsa 165 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800503": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800503": ""
          },
          "productId": "0110120968400224___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Almendras",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,02 &euro;",
            "PUMPriceFormatted": "26,93 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/22/4/0120968400224/0120968400224000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/22/4/0120968400224/0120968400224000p01011.jpg"
          },
          "description": "EAGLE almendras mediterráneas saladas bolsa 100 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Almendras"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120952900528___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Aperitivos diversos y snacks",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,14 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/52/8/0120952900528/0120952900528000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/52/8/0120952900528/0120952900528000p01011.jpg"
          },
          "description": "FACUNDO Chaskis snack de maíz bolsa 100 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Aperitivos diversos y snacks"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800501": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800501": ""
          },
          "productId": "0110120952300190___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cacahuetes",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,72 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "14,88 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/19/0/0120952300190/0120952300190000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/19/0/0120952300190/0120952300190000p01011.jpg"
          },
          "description": "EAGLE cacahuetes fritos con miel lata 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800502": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800502": ""
          },
          "productId": "0110120952300547___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cacahuetes",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,22 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "16,27 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/54/7/0120952300547/0120952300547000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/54/7/0120952300547/0120952300547000p01011.jpg"
          },
          "description": "EAGLE cacahuetes dorados a la miel ligeramente salados bolsa 75 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Cacahuetes"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800902": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800902": ""
          },
          "productId": "0110120977800471___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cocktails",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,45 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "13,61 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/47/1/0120977800471/0120977800471000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/47/1/0120977800471/0120977800471000p01011.jpg"
          },
          "description": "MATUTANO cóctel de frutos secos tostados al horno bolsa 180 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800507": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800507": ""
          },
          "productId": "0110120977800224___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cocktails",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "16,40 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/22/4/0120977800224/0120977800224000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/22/4/0120977800224/0120977800224000p01011.jpg"
          },
          "description": "EAGLE Cóctel Musik de frutos secos selectos lata 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800505": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800505": ""
          },
          "productId": "0110120977800216___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cocktails",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,78 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,70 &euro;",
            "PUMPriceFormatted": "18,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/21/6/0120977800216/0120977800216000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/21/6/0120977800216/0120977800216000p01011.jpg"
          },
          "description": "EAGLE cóctel de frutos secos deluxe lata 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800508": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800508": ""
          },
          "productId": "0110120977800935___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cocktails",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,47 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "19,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/93/5/0120977800935/0120977800935000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/93/5/0120977800935/0120977800935000p01011.jpg"
          },
          "description": "EAGLE cóctel de frutos secos fritos con miel bolsa 75 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120977800109___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cocktails",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,03 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,98 &euro;",
            "PUMPriceFormatted": "8,17 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/10/9/0120977800109/0120977800109000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/10/9/0120977800109/0120977800109000p01011.jpg"
          },
          "description": "GREFUSA MISTER CORN Grefusitos cóctel frutos secos Mix 5 bolsa 120 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Cocktails"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800506": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800506": ""
          },
          "productId": "0110120971600216___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Frutas y frutos secos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,64 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,61 &euro;",
            "PUMPriceFormatted": "21,47 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/21/6/0120971600216/0120971600216000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/21/6/0120971600216/0120971600216000p01011.jpg"
          },
          "description": "EAGLE anacardos de la India bolsa 75 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Frutas y frutos secos diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800701": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800701": ""
          },
          "productId": "0110120974000182___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Palomitas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,60 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "8,67 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/18/2/0120974000182/0120974000182000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/18/2/0120974000182/0120974000182000p01011.jpg"
          },
          "description": "POPITAS sabor natural con sal palomitas para microondas pack 3x100 g estuche 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Palomitas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914142800201": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914142800201": ""
          },
          "productId": "0110120952600649___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "11,11 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/64/9/0120952600649/0120952600649000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/64/9/0120952600649/0120952600649000p01011.jpg"
          },
          "description": "LAY'S GOURMET patatas fritas crujientes bolsa 180 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800801": "Por la compra de 2 unidades regalo de una botella de Pepsi Max Zero Azúcar de 2 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800801": ""
          },
          "productId": "0110120950403293___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,95 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,50 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/29/3/0120950403293/0120950403293000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/29/3/0120950403293/0120950403293000p01011.jpg"
          },
          "description": "LAY'S patatas fritas al punto de sal bolsa 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800601": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914141800601": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120952800579___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "8,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/04/00200452800998____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/04/00200452800998____1__75x75.jpg"
          },
          "description": "VICENTE VIDAL RIO DE JANEIRO STYLE patatas fritas sabor lima envase 120 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800301": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800301": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120952900403___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "11,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/40/3/0120952900403/0120952900403000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/40/3/0120952900403/0120952900403000p01011.jpg"
          },
          "description": "PRINGLES patatas fritas original tubo 165 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800301": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800301": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120952900411___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "11,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/41/1/0120952900411/0120952900411000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/41/1/0120952900411/0120952900411000p01011.jpg"
          },
          "description": "PRINGLES patatas fritas paprika tubo 165",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800301": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800301": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120952900445___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "11,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/44/5/0120952900445/0120952900445000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/44/5/0120952900445/0120952900445000p01011.jpg"
          },
          "description": "PRINGLES patatas fritas Sour Cream & Onion tubo 165 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914141800301": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800301": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120952900452___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "11,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/45/2/0120952900452/0120952900452000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/45/2/0120952900452/0120952900452000p01011.jpg"
          },
          "description": "PRINGLES patatas fritas sabor aceite de oliva y romero tubo 165 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120952600805___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,88 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,79 &euro;",
            "PUMPriceFormatted": "10,53 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/80/5/0120952600805/0120952600805000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/80/5/0120952600805/0120952600805000p01011.jpg"
          },
          "description": "ESPADA patatas fritas con aceite de oliva bolsa 170 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120952800520___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Patatas fritas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,88 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "16 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/52/0/0120952800520/0120952800520000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/52/0/0120952800520/0120952800520000p01011.jpg"
          },
          "description": "TORRES Selecta patatas fritas con trufa envase 125 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Patatas fritas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120975900190___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pipas y kikos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,02 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,79 &euro;",
            "PUMPriceFormatted": "11,93 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/19/0/0120975900190/0120975900190000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/19/0/0120975900190/0120975900190000p01011.jpg"
          },
          "description": "GREFUSA EL PIPONAZO pipas al punto de sal bolsa 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120975900059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pipas y kikos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,30 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,79 &euro;",
            "PUMPriceFormatted": "11,93 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0120975900059/0120975900059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0120975900059/0120975900059000p01011.jpg"
          },
          "description": "GREFUSA EL PIPONAZO original pipas de girasol con sal bolsa 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Pipas y kikos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "20914141800504": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "20914141800504": ""
          },
          "productId": "0110120970200406___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Piñones y pistachos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,62 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,59 &euro;",
            "PUMPriceFormatted": "21,20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/40/6/0120970200406/0120970200406000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/40/6/0120970200406/0120970200406000p01011.jpg"
          },
          "description": "EAGLE pistachos de Irán salados bolsa 75 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914143800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914143800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120970200349___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Piñones y pistachos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,75 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/34/9/0120970200349/0120970200349000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/34/9/0120970200349/0120970200349000p01011.jpg"
          },
          "description": "WONDERFUL pistacho tostado salado envase 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914143800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914143800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120970200356___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Piñones y pistachos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,75 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/35/6/0120970200356/0120970200356000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/35/6/0120970200356/0120970200356000p01011.jpg"
          },
          "description": "WONDERFUL pistachos con sal y pimienta envase 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "20914143800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "20914143800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110120970200364___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Piñones y pistachos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,75 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/36/4/0120970200364/0120970200364000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/36/4/0120970200364/0120970200364000p01011.jpg"
          },
          "description": "WONDERFUL pistachos sin sal envase 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Aperitivos y frutos secos > Piñones y pistachos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800003": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800003": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118005500352___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz especialidades",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,74 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,94 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/13/1/0118005801131/0118005801131000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/13/1/0118005801131/0118005801131000p01011.jpg"
          },
          "description": "GALLINA BLANCA IDEAS AL PLATO preparado para cocinar risotto a la parmesana sobre 175 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800003": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800003": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118005500360___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz especialidades",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,75 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,74 &euro;",
            "PUMPriceFormatted": "9,94 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/16/4/0118005801164/0118005801164000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/16/4/0118005801164/0118005801164000p01011.jpg"
          },
          "description": "GALLINA BLANCA IDEAS AL PLATO preparado para cocinar risotto de setas sobre 175 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118005700432___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz especialidades",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,25 &euro;",
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/43/2/0118005700432/0118005700432000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/43/2/0118005700432/0118005700432000p01011.jpg"
          },
          "description": "NOMEN arroz bomba selección paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118005800521___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz especialidades",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,09 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/52/1/0118005800521/0118005800521000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/52/1/0118005800521/0118005800521000p01011.jpg"
          },
          "description": "NOMEN arroz de trigo tierno caja 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118005801669___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz especialidades",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,49 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,25 &euro;",
            "PUMPriceFormatted": "1,25 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/66/9/0118005801669/0118005801669000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/66/9/0118005801669/0118005801669000p01011.jpg"
          },
          "description": "EL CORTE INGLES arroz redondo integral paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Arroz > Arroz especialidades"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118005600202___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz grano largo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,24 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,20 &euro;",
            "PUMPriceFormatted": "1,20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/20/2/0118005600202/0118005600202000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/20/2/0118005600202/0118005600202000p01011.jpg"
          },
          "description": "NOMEN arroz grano largo paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Arroz > Arroz grano largo"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118005700556___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Arroz grano redondo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,42 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,35 &euro;",
            "PUMPriceFormatted": "1,35 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/55/6/0118005700556/0118005700556000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/55/6/0118005700556/0118005700556000p01011.jpg"
          },
          "description": "NOMEN arroz redondo extra paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Arroz > Arroz grano redondo"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120930800162___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Edulcorantes y fructosas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,90 &euro;",
            "PUMPriceFormatted": "0,01 &euro; / Dosis"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/16/2/0120930800162/0120930800162000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/16/2/0120930800162/0120930800162000p01011.jpg"
          },
          "description": "CANDEREL edulcorante de aspartamo dosificador 350 comprimidos",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Azúcar y edulcorantes > Edulcorantes y fructosas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118089300059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,91 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,75 &euro;",
            "PUMPriceFormatted": "8,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118089300059/0118089300059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118089300059/0118089300059000p01011.jpg"
          },
          "description": "KUNG-FU fideos sabor pollo paquete 85 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118089300042___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,91 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,75 &euro;",
            "PUMPriceFormatted": "8,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/04/2/0118089300042/0118089300042000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/04/2/0118089300042/0118089300042000p01011.jpg"
          },
          "description": "KUNG-FU fideos sabor gambas paquete 85 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118036001099___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,29 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,28 &euro;",
            "PUMPriceFormatted": "20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//01/09/9/0118036001099/0118036001099000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//01/09/9/0118036001099/0118036001099000p01011.jpg"
          },
          "description": "MARUCHAN fideos con gambas envase 64 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118036001081___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,29 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,28 &euro;",
            "PUMPriceFormatted": "20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//01/08/1/0118036001081/0118036001081000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//01/08/1/0118036001081/0118036001081000p01011.jpg"
          },
          "description": "MARUCHAN fideos con pollo envase 64 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800006": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800006": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118036001065___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/06/5/0118036001065/0118036001065000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/06/5/0118036001065/0118036001065000p01011.jpg"
          },
          "description": "GALLINA BLANCA YATEKOMO fideos orientales instantáneos curry picante listos en 3 minutos vaso 61 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800006": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800006": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118036001073___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/07/3/0118036001073/0118036001073000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/07/3/0118036001073/0118036001073000p01011.jpg"
          },
          "description": "GALLINA BLANCA YATEKOMO fideos orientales instantáneos sabor gamba listos en 3 minutos vaso 60 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800006": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800006": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118036001107___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,64 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//01/10/7/0118036001107/0118036001107000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//01/10/7/0118036001107/0118036001107000p01011.jpg"
          },
          "description": "GALLINA BLANCA YATEKOMO fideos orientales instantáneos con verduras listos en 3 minutos vaso 59 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800006": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800006": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118036001115___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//01/11/5/0118036001115/0118036001115000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//01/11/5/0118036001115/0118036001115000p01011.jpg"
          },
          "description": "GALLINA BLANCA YATEKOMO fideos orientales instantáneos listos en 3 minutos vaso 61 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800006": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800006": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118036001123___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Comida oriental",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,10 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//01/12/3/0118036001123/0118036001123000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//01/12/3/0118036001123/0118036001123000p01011.jpg"
          },
          "description": "GALLINA BLANCA YATEKOMO fideos orientales instantáneos sabor pollo listos en 3 minutos vaso 60 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Comida Internacional > Comida oriental"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118089300380___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas orientales",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,33 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,35 &euro;",
            "PUMPriceFormatted": "22,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118089300380/0118089300380000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118089300380/0118089300380000p01011.jpg"
          },
          "description": "BLUE DRAGON salsa teriyaki botella 150 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118089300034___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas orientales",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,93 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,75 &euro;",
            "PUMPriceFormatted": "11,67 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/03/4/0118089300034/0118089300034000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/03/4/0118089300034/0118089300034000p01011.jpg"
          },
          "description": "KIM VE WONG salsa de soja frasco 150 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Comida Internacional > Salsas orientales"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002600346___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Magro de cerdo en lata",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,50 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,36 &euro;",
            "PUMPriceFormatted": "8,40 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/34/6/0118002600346/0118002600346000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/34/6/0118002600346/0118002600346000p01011.jpg"
          },
          "description": "APIS magro de cerdo pack ahorro 2 latas 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de carne > Magro de cerdo en lata"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002500033___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salchichas en conserva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,78 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,24 &euro;",
            "PUMPriceFormatted": "20,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/03/3/0118002500033/0118002500033000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/03/3/0118002500033/0118002500033000p01011.jpg"
          },
          "description": "MEICA salchichas 60 piezas aperitivo frasco 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002500025___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salchichas en conserva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,51 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "15 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/5/0118002500025/0118002500025000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/5/0118002500025/0118002500025000p01011.jpg"
          },
          "description": "MEICA salchichas Bockwurst 6 piezas frasco 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002500017___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salchichas en conserva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,55 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,94 &euro;",
            "PUMPriceFormatted": "23,76 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/01/7/0118002500017/0118002500017000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/01/7/0118002500017/0118002500017000p01011.jpg"
          },
          "description": "MEICA salchichas frankfurt 6 piezas frasco 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002500744___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salchichas en conserva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,75 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,35 &euro;",
            "PUMPriceFormatted": "9,40 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/74/4/0118002500744/0118002500744000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/74/4/0118002500744/0118002500744000p01011.jpg"
          },
          "description": "BOKLUNDER salchichas alemanas auténticas cóctel 36 unidades frasco 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de carne > Salchichas en conserva"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118031701719___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Dulces de membrillo y frutas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,52 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,25 &euro;",
            "PUMPriceFormatted": "5,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/71/9/0118031701719/0118031701719000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/71/9/0118031701719/0118031701719000p01011.jpg"
          },
          "description": "EL QUIJOTE membrillo natural 100% tarrina 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de frutas > Dulces de membrillo y frutas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118030301842___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Frutas diversas almíbar",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,46 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,45 &euro;",
            "PUMPriceFormatted": "22,71 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/84/2/0118030301842/0118030301842000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/84/2/0118030301842/0118030301842000p01011.jpg"
          },
          "description": "CUEVAS castañas en  almíbar ligero frasco 240 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de frutas > Frutas diversas almíbar"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118031701503___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Frutas selectas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "12,97 &euro;",
            "onsale": true,
            "salePriceFormatted": "10,50 &euro;",
            "PUMPriceFormatted": "35 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/50/3/0118031701503/0118031701503000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/50/3/0118031701503/0118031701503000p01011.jpg"
          },
          "description": "CUEVAS marrón glace frasco 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de frutas > Frutas selectas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118030800074___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Melocotón en almíbar",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,68 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,45 &euro;",
            "PUMPriceFormatted": "3,02 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/07/4/0118030800074/0118030800074000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/07/4/0118030800074/0118030800074000p01011.jpg"
          },
          "description": "EL CORTE INGLES melocotón en almíbar en mitades 6-8 piezas lata 480 g peso neto",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de frutas > Melocotón en almíbar"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118008500029___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Almejas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,87 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,50 &euro;",
            "PUMPriceFormatted": "84,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118008500029/0118008500029000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118008500029/0118008500029000p01011.jpg"
          },
          "description": "CUCA almejas al natural coreanas lata 65 g neto escurrido 11-15 piezas",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Almejas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800010": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800010": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118010800078___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Anchoas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "103,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/07/8/0118010800078/0118010800078000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/07/8/0118010800078/0118010800078000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO filetes de anchoa en aceite de oliva lata 29 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18014146800010": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800010": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118010801068___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Anchoas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,10 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,99 &euro;",
            "PUMPriceFormatted": "103,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/06/8/0118010801068/0118010801068000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/06/8/0118010801068/0118010801068000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO filetes de anchoa a la antigua en aceite de oliva virgen extra lata 29 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800010": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800010": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118010801050___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Anchoas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,60 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,99 &euro;",
            "PUMPriceFormatted": "103,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/05/0/0118010801050/0118010801050000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/05/0/0118010801050/0118010801050000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO filetes de anchoa en aceite de oliva ecológico lata 29 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            },
            {
              "description": "Producto ecológico",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_ecologico.gif"
            }
          ]
        }
      ],
      "categoria": "Conservas de pescado > Anchoas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": false,
          "promotionLinksMap": {},
          "productId": "0110118006600417___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Atún en aceite de oliva",
          "tempOfferDescription": "con 1 lata de regalo",
          "priceInfo": {
            "basePriceFormatted": "4,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/41/7/0118006600417/0118006600417000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/41/7/0118006600417/0118006600417000p01011.jpg"
          },
          "description": "ALBO atún claro en aceite de oliva pack 3 latas 67 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118006600318___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Atún en aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,90 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,99 &euro;",
            "PUMPriceFormatted": "20,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/31/8/0118006600318/0118006600318000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/31/8/0118006600318/0118006600318000p01011.jpg"
          },
          "description": "ALBO atún claro en aceite de oliva pack 6 latas 48 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": false,
          "promotionLinksMap": {},
          "productId": "0110118006600425___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Atún en aceite de oliva",
          "tempOfferDescription": "con 1 lata de regalo",
          "priceInfo": {
            "basePriceFormatted": "4,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "18,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/42/5/0118006600425/0118006600425000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/42/5/0118006600425/0118006600425000p01011.jpg"
          },
          "description": "ALBO atún claro en aceite oliva virgen pack 3 latas 67 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118006600029___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Atún en aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,90 &euro;",
            "PUMPriceFormatted": "19,40 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118006600029/0118006600029000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118006600029/0118006600029000p01011.jpg"
          },
          "description": "CUCA atún claro en aceite de oliva pack 3 latas 67 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118006600532___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Atún en aceite de oliva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,49 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,99 &euro;",
            "PUMPriceFormatted": "14,88 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/53/2/0118006600532/0118006600532000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/53/2/0118006600532/0118006600532000p01011.jpg"
          },
          "description": "TEJERO atún de almadraba en aceite de oliva pack 3 latas 67 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Atún en aceite de oliva"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118011300060___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Berberechos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,90 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,50 &euro;",
            "PUMPriceFormatted": "38,46 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/0/0118011300060/0118011300060000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/0/0118011300060/0118011300060000p01011.jpg"
          },
          "description": "NOLY berberechos de Holanda al natural lata 65 g neto escurrido 45-55 piezas",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Berberechos diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700219___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "10,73 &euro;",
            "onsale": true,
            "salePriceFormatted": "7,99 &euro;",
            "PUMPriceFormatted": "28,54 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/21/9/0118012700219/0118012700219000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/21/9/0118012700219/0118012700219000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte en aceite de oliva en lomos tarro 280 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800009": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800009": ""
          },
          "productId": "0110118012700094___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,63 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,60 &euro;",
            "PUMPriceFormatted": "32 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/09/4/0118012700094/0118012700094000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/09/4/0118012700094/0118012700094000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte en aceite de oliva lata 175 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700086___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,44 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "24,39 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118012700086/0118012700086000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118012700086/0118012700086000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte en aceite de oliva lata 82 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700615___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "7,02 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,99 &euro;",
            "PUMPriceFormatted": "23,04 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/61/5/0118012700615/0118012700615000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/61/5/0118012700615/0118012700615000p01011.jpg"
          },
          "description": "FRINSA bonito del norte en aceite de oliva frasco 260 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Bonito en aceite"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700060___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en escabeche",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "7,29 &euro;",
            "onsale": true,
            "salePriceFormatted": "6,90 &euro;",
            "PUMPriceFormatted": "26,54 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/0/0118012700060/0118012700060000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/0/0118012700060/0118012700060000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte frito en escabeche lata 260 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700052___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en escabeche",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,01 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,69 &euro;",
            "PUMPriceFormatted": "26,36 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/2/0118012700052/0118012700052000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/2/0118012700052/0118012700052000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte frito en escabeche lata 140 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118012700045___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Bonito en escabeche",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,44 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "24,39 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/04/5/0118012700045/0118012700045000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/04/5/0118012700045/0118012700045000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO bonito del norte en escabeche lata 82 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Bonito en escabeche"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118014600029___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Chipirones",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,82 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,25 &euro;",
            "PUMPriceFormatted": "59,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118014600029/0118014600029000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/9/0118014600029/0118014600029000p01011.jpg"
          },
          "description": "ALBO chipirones rellenos en su tinta lata 72 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118014600102___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Chipirones",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,82 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,25 &euro;",
            "PUMPriceFormatted": "59,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/10/2/0118014600102/0118014600102000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/10/2/0118014600102/0118014600102000p01011.jpg"
          },
          "description": "ALBO chipirones rellenos en aceite de oliva lata 72 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118014600110___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Chipirones",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,82 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,25 &euro;",
            "PUMPriceFormatted": "59,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118014600110/0118014600110000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118014600110/0118014600110000p01011.jpg"
          },
          "description": "ALBO chipirones rellenos en salsa americana lata 72 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Chipirones"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118022600086___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Conservas de pescado diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,31 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "44,22 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118022600086/0118022600086000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118022600086/0118022600086000p01011.jpg"
          },
          "description": "UBAGO langostillos al Natural lata 45 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Conservas de pescado diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029300086___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Huevas de pescado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,63 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "9,95 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118029300086/0118029300086000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118029300086/0118029300086000p01011.jpg"
          },
          "description": "UBAGO huevas de bacalao lata 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Huevas de pescado"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029300136___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Hígado de bacalao",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,80 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,79 &euro;",
            "PUMPriceFormatted": "29,83 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/13/6/0118029300136/0118029300136000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/13/6/0118029300136/0118029300136000p01011.jpg"
          },
          "description": "UBAGO hígado de bacalao ahumado en su propio aceite lata 60 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Hígado de bacalao"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118015100037___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Mejillones en escabeche",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,86 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,79 &euro;",
            "PUMPriceFormatted": "40,43 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/03/7/0118015100037/0118015100037000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/03/7/0118015100037/0118015100037000p01011.jpg"
          },
          "description": "CUCA mejillones fritos en escabeche 12-16 piezas lata 69 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Mejillones en escabeche"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118020700078___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Melva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,84 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "23,41 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/07/8/0118020700078/0118020700078000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/07/8/0118020700078/0118020700078000p01011.jpg"
          },
          "description": "EL CORTE INGLES melva canutera en aceite de oliva lata 85 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118020700110___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Melva",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,04 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,39 &euro;",
            "PUMPriceFormatted": "16,35 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118020700110/0118020700110000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118020700110/0118020700110000p01011.jpg"
          },
          "description": "EL CORTE INGLES melva de Almadraba en aceite de oliva lata 85 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Melva"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016500011___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sardinas en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,53 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,49 &euro;",
            "PUMPriceFormatted": "17,53 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/01/1/0118016500011/0118016500011000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/01/1/0118016500011/0118016500011000p01011.jpg"
          },
          "description": "CUCA sardinas en aceite de oliva lata 85 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016500326___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sardinas en aceite",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,75 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "20 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/32/6/0118016500326/0118016500326000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/32/6/0118016500326/0118016500326000p01011.jpg"
          },
          "description": "ORTIZ EL VELERO sardinas en aceite de oliva a la antigua lata 100 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Sardinas en aceite"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800008": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800008": ""
          },
          "productId": "0110118004700185___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sardinillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,72 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,63 &euro;",
            "PUMPriceFormatted": "19,18 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/18/5/0118004700185/0118004700185000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/18/5/0118004700185/0118004700185000p01011.jpg"
          },
          "description": "CABO DE PEÑAS sardinillas en aceite de oliva 7-10 piezas lata 85 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118004700110___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sardinillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,08 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,79 &euro;",
            "PUMPriceFormatted": "21,31 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118004700110/0118004700110000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/0/0118004700110/0118004700110000p01011.jpg"
          },
          "description": "USISA sardinillas de la costa en aceite de oliva lata 84 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Sardinillas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118006200069___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ventresca de atún",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,11 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,10 &euro;",
            "PUMPriceFormatted": "52,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/9/0118006200069/0118006200069000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/9/0118006200069/0118006200069000p01011.jpg"
          },
          "description": "ESCURIS ventresca de atún claro en aceite de oliva lata 78 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas de pescado > Ventresca de atún"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900513___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Acelgas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,13 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "4,59 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900513____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900513____1__75x75.jpg"
          },
          "description": "GVTARRA acelgas troceadas al natural frasco 425 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Acelgas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118021800083___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Alcachofas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,98 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,35 &euro;",
            "PUMPriceFormatted": "13,38 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118021800083____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118021800083____1__75x75.jpg"
          },
          "description": "GVTARRA alcachofas corazones en mitades frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118021800265___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Alcachofas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,43 &euro;",
            "onsale": true,
            "salePriceFormatted": "5,95 &euro;",
            "PUMPriceFormatted": "14,88 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/26/5/0118021800265/0118021800265000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/26/5/0118021800265/0118021800265000p01011.jpg"
          },
          "description": "PEDRO LUIS corazones de alcachofa D.O. Tudela frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118021800752___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Alcachofas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "12,19 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/75/2/0118021800752/0118021800752000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/75/2/0118021800752/0118021800752000p01011.jpg"
          },
          "description": "EL CORTE INGLES corazones de alcachofa extra al natural frasco 160 g neto escurrido 15-20 piezas",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Alcachofas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600372___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Apio",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,89 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,85 &euro;",
            "PUMPriceFormatted": "4,72 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/37/2/0118025600372/0118025600372000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/37/2/0118025600372/0118025600372000p01011.jpg"
          },
          "description": "EL CORTE INGLES apio rallado para ensaladas frasco 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Apio"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900323___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cardos y borraja",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,16 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,35 &euro;",
            "PUMPriceFormatted": "8,38 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900323____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900323____1__75x75.jpg"
          },
          "description": "GVTARRA borraja al natural troceada frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900331___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Cardos y borraja",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,44 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,65 &euro;",
            "PUMPriceFormatted": "6,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900331____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900331____1__75x75.jpg"
          },
          "description": "GVTARRA cardo al natural troceado frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Cardos y borraja"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118022300679___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Champiñón",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,55 &euro;",
            "PUMPriceFormatted": "8,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/67/9/0118022300679/0118022300679000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/67/9/0118022300679/0118022300679000p01011.jpg"
          },
          "description": "GVTARRA champiñón extra laminado pack 3 latas 105 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118022300026___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Champiñón",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,11 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "5,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/02/6/0118022300026/0118022300026000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/02/6/0118022300026/0118022300026000p01011.jpg"
          },
          "description": "EL CORTE INGLES champiñon laminado extra lata 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Champiñón"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600414___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,52 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,29 &euro;",
            "PUMPriceFormatted": "7,17 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/41/4/0118025600414/0118025600414000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/41/4/0118025600414/0118025600414000p01011.jpg"
          },
          "description": "EL CORTE INGLES ensalada oriental frasco 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600406___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,52 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,29 &euro;",
            "PUMPriceFormatted": "7,17 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/40/6/0118025600406/0118025600406000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/40/6/0118025600406/0118025600406000p01011.jpg"
          },
          "description": "EL CORTE INGLES ensalada tropical frasco 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Ensaladas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900315___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espinacas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,30 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "4,59 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900315____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900315____1__75x75.jpg"
          },
          "description": "GVTARRA espinacas extra al natural frasco 425 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Espinacas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118023702352___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "+regalo de aceite de oliva La Española 500 ml+vinagre D.O. Jerez Coosur 250ml",
          "priceInfo": {
            "basePriceFormatted": "16,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "13,95 &euro;",
            "PUMPriceFormatted": "18,60 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/35/2/0118023702352/0118023702352000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/35/2/0118023702352/0118023702352000p01011.jpg"
          },
          "description": "CARRETILLA espárragos blancos extra 5 -7  piezas pack 3 latas 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023700224___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "13,70 &euro;",
            "onsale": true,
            "salePriceFormatted": "12,95 &euro;",
            "PUMPriceFormatted": "25,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/22/4/0118023700224/0118023700224000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/22/4/0118023700224/0118023700224000p01011.jpg"
          },
          "description": "EL CORTE INGLES espárragos blancos extra gruesos D.O. Navarra 7-9 piezas lata 500 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023700430___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "11,90 &euro;",
            "onsale": true,
            "salePriceFormatted": "10,85 &euro;",
            "PUMPriceFormatted": "21,70 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/43/0/0118023700430/0118023700430000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/43/0/0118023700430/0118023700430000p01011.jpg"
          },
          "description": "EL CORTE INGLES espárragos blancos extra gruesos calidad extra 9-12 piezas lata 500 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023701461___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,60 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,95 &euro;",
            "PUMPriceFormatted": "19,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/46/1/0118023701461/0118023701461000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/46/1/0118023701461/0118023701461000p01011.jpg"
          },
          "description": "EL CORTE INGLES espárragos blancos muy gruesos D.O. Navarra 6-8 piezas lata 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023700406___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,45 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "18,29 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/40/6/0118023700406/0118023700406000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/40/6/0118023700406/0118023700406000p01011.jpg"
          },
          "description": "EL CORTE INGLES espárragos blancos gruesos extra D.O. Navarra 7-10 piezas frasco 205 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023700257___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,10 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,75 &euro;",
            "PUMPriceFormatted": "22 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/25/7/0118023700257/0118023700257000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/25/7/0118023700257/0118023700257000p01011.jpg"
          },
          "description": "EL CORTE INGLES espárragos blancos D.O. Navarra 6-12 piezas lata 125 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118023700562___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espárragos blancos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,74 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "15 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/56/2/0118023700562/0118023700562000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/56/2/0118023700562/0118023700562000p01011.jpg"
          },
          "description": "LODOSA espárragos blancos D.O. Navarra 12-16 piezas lata 250 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Espárragos blancos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118024200166___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Guisantes",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,74 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,45 &euro;",
            "PUMPriceFormatted": "4,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/16/6/0118024200166/0118024200166000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/16/6/0118024200166/0118024200166000p01011.jpg"
          },
          "description": "EL CORTE INGLES guisantes al natural muy finos pack 3 latas 120 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Guisantes"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118042100075___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Judías verdes",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,30 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "5,57 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118042100075____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118042100075____1__75x75.jpg"
          },
          "description": "GVTARRA judías verdes planas extra cortadas en trozos frasco 350 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118042100083___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Judías verdes",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,25 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "5,57 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118042100083____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118042100083____1__75x75.jpg"
          },
          "description": "GVTARRA judías verdes cortadas en tiras frasco 350 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Judías verdes"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118049700059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Maíz",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,09 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,94 &euro;",
            "PUMPriceFormatted": "4,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118049700059/0118049700059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118049700059/0118049700059000p01011.jpg"
          },
          "description": "EL CORTE INGLES maíz dulce en grano pack 3 latas 140 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Maíz"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118028000067___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Menestras y macedonias",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,14 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,45 &euro;",
            "PUMPriceFormatted": "5,76 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118028000067____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118028000067____1__75x75.jpg"
          },
          "description": "GVTARRA menestra jardinera de verduras frasco 425 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Menestras y macedonias"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118026100976___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pimientos del bierzo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,76 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "9,05 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/97/6/0118026100976/0118026100976000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/97/6/0118026100976/0118026100976000p01011.jpg"
          },
          "description": "IBSA pimientos del Bierzo asados en horno pelados a mano dulces con aceite de oliva frasco 220 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Pimientos del bierzo"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118026100174___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pimientos del piquillo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,88 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,10 &euro;",
            "PUMPriceFormatted": "14,76 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/18/00118026100174____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/18/00118026100174____1__75x75.jpg"
          },
          "description": "LODOSA pimientos del piquillo extra D.O. Lodosa lata 210 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Pimientos del piquillo"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600364___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Remolacha",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,89 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,85 &euro;",
            "PUMPriceFormatted": "4,72 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/36/4/0118025600364/0118025600364000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/36/4/0118025600364/0118025600364000p01011.jpg"
          },
          "description": "EL CORTE INGLES remolacha rallada especial para ensaladas frasco 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Remolacha"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800001": ""
          },
          "productId": "0110118043500018___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "2,46 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/01/8/0118043500018/0118043500018000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/01/8/0118043500018/0118043500018000p01011.jpg"
          },
          "description": "HELIOS tomate frito frasco 570 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043500893___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,65 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,58 &euro;",
            "PUMPriceFormatted": "2,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/89/3/0118043500893/0118043500893000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/89/3/0118043500893/0118043500893000p01011.jpg"
          },
          "description": "HELIOS tomate frito estilo Mediterráneo con aceite oliva virgen extra frasco 560 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043500059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,67 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,35 &euro;",
            "PUMPriceFormatted": "2,37 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/05/9/0118043500059/0118043500059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/05/9/0118043500059/0118043500059000p01011.jpg"
          },
          "description": "HELIOS salsa de tomate casero frasco 570 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043501263___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,50 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "2,68 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/26/3/0118043501263/0118043501263000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/26/3/0118043501263/0118043501263000p01011.jpg"
          },
          "description": "EL CORTE INGLES tomate frito cosecha pack ahorro 4 frasco 350 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Tomate frito"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118027500117___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate natural triturado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,71 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,65 &euro;",
            "PUMPriceFormatted": "1,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/7/0118027500117/0118027500117000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/7/0118027500117/0118027500117000p01011.jpg"
          },
          "description": "EL CORTE INGLES tomate natural triturado calidad extra lata 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Tomate natural triturado"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900596___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vegetales diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,30 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "4,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/59/6/0118029900596/0118029900596000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/59/6/0118029900596/0118029900596000p01011.jpg"
          },
          "description": "GVTARRA patatas enteras frasco 450 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900372___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vegetales diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,38 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,85 &euro;",
            "PUMPriceFormatted": "23,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/37/2/0118029900372/0118029900372000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/37/2/0118029900372/0118029900372000p01011.jpg"
          },
          "description": "GVTARRA habitas tiernas extra finas frasco 210 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600380___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vegetales diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,85 &euro;",
            "PUMPriceFormatted": "5 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118025600380/0118025600380000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118025600380/0118025600380000p01011.jpg"
          },
          "description": "EL CORTE INGLES brotes germinados para ensaladas frasco 170 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029902519___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tortillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,38 &euro;",
            "PUMPriceFormatted": "6,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/51/9/0118029902519/0118029902519000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/51/9/0118029902519/0118029902519000p01011.jpg"
          },
          "description": "HELIOS Torti-ya preparado para tortilla de patata con cebolla frasco 390 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029902659___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Vegetales diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,83 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,39 &euro;",
            "PUMPriceFormatted": "5,79 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/65/9/0118029902659/0118029902659000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/65/9/0118029902659/0118029902659000p01011.jpg"
          },
          "description": "IBSA cebolla caramelizada frasco 240 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Vegetales diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118023700554___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Yemas de espárragos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,26 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,85 &euro;",
            "PUMPriceFormatted": "28,52 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/55/4/0118023700554/0118023700554000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/55/4/0118023700554/0118023700554000p01011.jpg"
          },
          "description": "LODOSA yemas de esparragos gruesos I.G.P. Navarra lata 135 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800201": "Por compras superiores a 6,00 € en espárragos regalo de 1 mayonesa El Corte Inglés"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800201": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118023700331___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Yemas de espárragos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,75 &euro;",
            "PUMPriceFormatted": "35,19 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/33/1/0118023700331/0118023700331000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/33/1/0118023700331/0118023700331000p01011.jpg"
          },
          "description": "EL CORTE INGLES yemas de espárragos blancos muy gruesos I.G.P. Navarra lata 135 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Yemas de espárragos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029900398___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Zanahorias",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,89 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,50 &euro;",
            "PUMPriceFormatted": "7,14 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900398____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/23/00118029900398____1__75x75.jpg"
          },
          "description": "GVTARRA zanahoria baby muy fina extra frasco 210 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118025600356___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Zanahorias",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,89 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,85 &euro;",
            "PUMPriceFormatted": "4,72 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/35/6/0118025600356/0118025600356000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/35/6/0118025600356/0118025600356000p01011.jpg"
          },
          "description": "EL CORTE INGLES zanahoria rallada para ensaladas frasco 180 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Conservas vegetales > Zanahorias"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118000100091___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Caldo de carne concentrado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,79 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,65 &euro;",
            "PUMPriceFormatted": "11,04 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/09/1/0118000100091/0118000100091000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/09/1/0118000100091/0118000100091000p01011.jpg"
          },
          "description": "KNORR STARLUX doble caldo de carne con aceite de oliva 24 pastillas estuche 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Gazpacho, caldos, sopas y purés > Caldo de carne concentrado"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118000100059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Caldo de pollo concentrado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,68 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,50 &euro;",
            "PUMPriceFormatted": "9,92 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118000100059/0118000100059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/05/9/0118000100059/0118000100059000p01011.jpg"
          },
          "description": "GALLINA BLANCA AVECREM caldo de pollo 24 pastillas estuche 252 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Gazpacho, caldos, sopas y purés > Caldo de pollo concentrado"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118851200198___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gazpacho y salmorejo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,25 &euro;",
            "PUMPriceFormatted": "3,25 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/19/8/0118851200198/0118851200198000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/19/8/0118851200198/0118851200198000p01011.jpg"
          },
          "description": "ALVALLE gazpacho natural envase 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118851200420___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gazpacho y salmorejo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,25 &euro;",
            "PUMPriceFormatted": "3,25 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/42/0/0118851200420/0118851200420000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/42/0/0118851200420/0118851200420000p01011.jpg"
          },
          "description": "ALVALLE gazpacho suave envase 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800201": "Con regalo de gazpacho El Corte Ingles envase 1 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800201": ""
          },
          "productId": "0110118851201154___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gazpacho y salmorejo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,31 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "2,31 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/15/4/0118851201154/0118851201154000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/15/4/0118851201154/0118851201154000p01011.jpg"
          },
          "description": "EL CORTE INGLES gazpacho de verduras seleccionadas envase 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            },
            {
              "description": "Producto sin gluten",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_singluten.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814155800601": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18814155800601": ""
          },
          "productId": "0110118851201170___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gazpacho y salmorejo",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,45 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,29 &euro;",
            "PUMPriceFormatted": "2,29 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/17/0/0118851201170/0118851201170000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/17/0/0118851201170/0118851201170000p01011.jpg"
          },
          "description": "EL CORTE INGLES salmorejo receta tradicional envase 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Gazpacho, caldos, sopas y purés > Gazpacho y salmorejo"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800002": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800002": ""
          },
          "productId": "0110118002002725___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sopas, caldos y cremas en brik",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,70 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,50 &euro;",
            "PUMPriceFormatted": "2,50 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/72/5/0118002002725/0118002002725000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/72/5/0118002002725/0118002002725000p01011.jpg"
          },
          "description": "GALLINA BLANCA caldo con sofrito para paella envase 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": false,
          "promotionLinksMap": {},
          "productId": "0110118002002584___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Sopas, caldos y cremas en brik",
          "tempOfferDescription": "+ 1 caldo para paella gratis",
          "priceInfo": {
            "basePriceFormatted": "7,18 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/58/4/0118002002584/0118002002584000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/58/4/0118002002584/0118002002584000p01011.jpg"
          },
          "description": "ANETO caldo natural de pollo pack 2 envases 1 l",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Gazpacho, caldos, sopas y purés > Sopas, caldos y cremas en brik"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18214154800002": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18214154800002": ""
          },
          "productId": "0110118282500067___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gulas y surimi refrigerado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "27 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/06/7/0118282500067/0118282500067000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/06/7/0118282500067/0118282500067000p01011.jpg"
          },
          "description": "LA GULA DEL NORTE fresca estuche 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18214154800001": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18214154800001": ""
          },
          "productId": "0110118282500158___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Gulas y surimi refrigerado",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,60 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,50 &euro;",
            "PUMPriceFormatted": "11,25 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/15/8/0118282500158/0118282500158000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/15/8/0118282500158/0118282500158000p01011.jpg"
          },
          "description": "KRISSIA surimi en barritas 24 unidades estuche 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Gulas y surimi refrigerado > Gulas y surimi refrigerado"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814136802001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814136802001": ""
          },
          "productId": "0110118840101036___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Huevos especiales",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,59 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "3,18 &euro; / Docena"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/03/6/0118840101036/0118840101036000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/03/6/0118840101036/0118840101036000p01011.jpg"
          },
          "description": "EL CORTE INGLES huevos de gallinas camperas estuche 6 unidades",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Huevos > Huevos especiales"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118007300413___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Alubias cocidas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,34 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "2,50 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/15/3/0118007000153/0118007000153000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/15/3/0118007000153/0118007000153000p01011.jpg"
          },
          "description": "CIDACOS alubia blanca cocida extra baja en sal frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Legumbres > Alubias cocidas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118006300232___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Alubias pintas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,44 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,75 &euro;",
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/23/2/0118006300232/0118006300232000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/23/2/0118006300232/0118006300232000p01011.jpg"
          },
          "description": "LA ASTURIANA alubia roja paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Legumbres > Alubias pintas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118008200034___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Garbanzos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,20 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "3,75 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/03/4/0118008200034/0118008200034000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/03/4/0118008200034/0118008200034000p01011.jpg"
          },
          "description": "LA ASTURIANA garbanzo lechoso paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118008200380___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Garbanzos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,01 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,75 &euro;",
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118008200380/0118008200380000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118008200380/0118008200380000p01011.jpg"
          },
          "description": "EL HOSTAL garbanzo extra paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Legumbres > Garbanzos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118007000104___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Garbanzos cocidos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,32 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "2,50 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/10/4/0118007000104/0118007000104000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/10/4/0118007000104/0118007000104000p01011.jpg"
          },
          "description": "CIDACOS garbanzos cocidos extra al natural frasco 400 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Legumbres > Garbanzos cocidos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118007700067___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Lentejas pardinas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,24 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/06/7/0118007700067/0118007700067000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/06/7/0118007700067/0118007700067000p01011.jpg"
          },
          "description": "LA ASTURIANA lenteja pardina extra paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118007700349___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Lentejas pardinas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,14 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,85 &euro;",
            "PUMPriceFormatted": null
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/34/9/0118007700349/0118007700349000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/34/9/0118007700349/0118007700349000p01011.jpg"
          },
          "description": "EL HOSTAL lenteja pardina extra paquete 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Legumbres > Lentejas pardinas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800004": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800004": ""
          },
          "productId": "0110118003501436___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Canelones",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "0,94 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "11,75 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/43/6/0118003501436/0118003501436000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/43/6/0118003501436/0118003501436000p01011.jpg"
          },
          "description": "EL PAVO canelones precocidos 12 placas envase 80 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Canelones"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800007": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800007": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118004403939___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espaguetis",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,27 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/93/9/0118004403939/0118004403939000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/93/9/0118004403939/0118004403939000p01011.jpg"
          },
          "description": "DE CECCO spaghetti nº12 bolsa 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800007": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800007": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118004403871___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Espaguetis",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,96 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/87/1/0118004403871/0118004403871000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/87/1/0118004403871/0118004403871000p01011.jpg"
          },
          "description": "DE CECCO spaguettini bolsa 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Espaguetis"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800005": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800005": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118003501444___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Lasaña",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,45 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "7,25 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/44/4/0118003501444/0118003501444000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/44/4/0118003501444/0118003501444000p01011.jpg"
          },
          "description": "EL PAVO lasaña precocida a las espinacas 18 placas envase 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800005": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800005": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118003501410___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Lasaña",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,45 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "7,25 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/41/0/0118003501410/0118003501410000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/41/0/0118003501410/0118003501410000p01011.jpg"
          },
          "description": "EL PAVO lasaña instantánea precocida 18 placas caja 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Lasaña"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800007": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800007": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118004403947___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Macarrones",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,27 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/94/7/0118004403947/0118004403947000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/94/7/0118004403947/0118004403947000p01011.jpg"
          },
          "description": "DE CECCO penne rigate nº41 bolsa 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Macarrones"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845101528___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/52/8/0118845101528/0118845101528000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/52/8/0118845101528/0118845101528000p01011.jpg"
          },
          "description": "GIOVANNI RANA GRAN RIPIENO pasta rellena de espinacas pasas y piñones envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845100587___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/58/7/0118845100587/0118845100587000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/58/7/0118845100587/0118845100587000p01011.jpg"
          },
          "description": "GIOVANNI RANA GOURMET pasta fresca rellena de setas y crema de queso envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845101189___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845101189____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845101189____1__75x75.jpg"
          },
          "description": "GIOVANNI RANA GOURMET pasta fresca rellena de queso de cabra y cebolla caramelizada envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845100629___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845100629____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845100629____1__75x75.jpg"
          },
          "description": "GIOVANNI RANA GOURMET ravioli fresco relleno de pera y crema de queso bandeja 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845100504___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845100504____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118845100504____1__75x75.jpg"
          },
          "description": "GIOVANNI RANA GOURMET tortellini fresco relleno de gorgonzola y nueces bandeja 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814128800101": "Con regalo de 1 queso rallado El Corte Ingles bolsa 100 g"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845102526___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pasta fresca",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,89 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "15,56 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//02/52/6/0118845102526/0118845102526000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//02/52/6/0118845102526/0118845102526000p01011.jpg"
          },
          "description": "GIOVANNI RANA GOURMET pasta fresca rellana de trufa blanca con setas y queso envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Pasta fresca"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800007": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800007": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118004403889___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pastas diversas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,98 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,95 &euro;",
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/88/9/0118004403889/0118004403889000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/88/9/0118004403889/0118004403889000p01011.jpg"
          },
          "description": "DE CECCO rigatoni nº24 bolsa 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800007": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800007": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118004403897___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pastas diversas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,95 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "3,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/89/7/0118004403897/0118004403897000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/89/7/0118004403897/0118004403897000p01011.jpg"
          },
          "description": "DE CECCO fusilli bolsa 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Pastas y masas > Pastas diversas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118553601107___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Empanadas y sandwiches",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,50 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,99 &euro;",
            "PUMPriceFormatted": "7,98 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/09/00118553601107____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/09/00118553601107____1__75x75.jpg"
          },
          "description": "EL CORTE INGLES rosca de jamón y queso pieza 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118551402383___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Empanadas y sandwiches",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "5,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "4,99 &euro;",
            "PUMPriceFormatted": "12,70 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/38/3/0118551402383/0118551402383000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/38/3/0118551402383/0118551402383000p01011.jpg"
          },
          "description": "EL CORTE INGLES emparedados 5 unidades calidad artesana bandeja 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614146805801": "Lleva una empanada gallega El Corte Inglés de 500 g + botella de Coca Cola clásica de 1,5 l por 6,95 €"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614146805801": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118558501096___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Empanadas y sandwiches",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,95 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "13,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/09/6/0118558501096/0118558501096000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/09/6/0118558501096/0118558501096000p01011.jpg"
          },
          "description": "EL CORTE INGLES empanada gallega de atún calidad artesana pieza 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614146805801": "Lleva una empanada gallega El Corte Inglés de 500 g + botella de Coca Cola clásica de 1,5 l por 6,95 €"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614146805801": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118558501062___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Empanadas y sandwiches",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,95 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "13,90 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/06/2/0118558501062/0118558501062000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/06/2/0118558501062/0118558501062000p01011.jpg"
          },
          "description": "EL CORTE INGLES Empanada gallega de ternera calidad artesana pieza 500 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118558503241___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Empanadas y sandwiches",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,60 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "8,84 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118558503241____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118558503241____1__75x75.jpg"
          },
          "description": "EL CORTE INGLÉS mini empanada de atún pieza 225 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Platos preparados > Empanadas y sandwiches"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814139801001": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814139801001": ""
          },
          "productId": "0110118851401531___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ensaladas preparadas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,91 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "7,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/53/1/0118851401531/0118851401531000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/53/1/0118851401531/0118851401531000p01011.jpg"
          },
          "description": "ARGAL ensalada rusa ligera con atún envase 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814139801002": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814139801002": ""
          },
          "productId": "0110118851401549___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ensaladas preparadas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,07 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "8,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/54/9/0118851401549/0118851401549000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/54/9/0118851401549/0118851401549000p01011.jpg"
          },
          "description": "ARGAL ensalada de cangrejo ligera envase 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Platos preparados > Ensaladas preparadas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814141800001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814141800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850402258___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,25 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,62 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/25/8/0118850402258/0118850402258000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/25/8/0118850402258/0118850402258000p01011.jpg"
          },
          "description": "PALACIOS LA ORIGINALE pizza masa fina barbacoa y de pollo envase 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814141800001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814141800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850402266___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,25 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,92 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/26/6/0118850402266/0118850402266000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/26/6/0118850402266/0118850402266000p01011.jpg"
          },
          "description": "PALACIOS LA ORIGINALE pizza de masa fina speciale con salsa pesto envase 380 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814141800001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814141800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850402274___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,25 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,25 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/27/4/0118850402274/0118850402274000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/27/4/0118850402274/0118850402274000p01011.jpg"
          },
          "description": "PALACIOS LA ORIGINALE pizza de masa fina con chorizo y queso de cabra envase 360 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400179___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,68 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/17/9/0118850400179/0118850400179000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/17/9/0118850400179/0118850400179000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza carbonara envase 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400229___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,59 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/22/9/0118850400229/0118850400229000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/22/9/0118850400229/0118850400229000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza de atún y bacon envase 405 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400245___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,59 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/24/5/0118850400245/0118850400245000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/24/5/0118850400245/0118850400245000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza de jamón y queso envase 405 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400286___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,59 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/28/6/0118850400286/0118850400286000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/28/6/0118850400286/0118850400286000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza rústica con bacon cebolla y mozzarella envase 405 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400237___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,85 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/23/7/0118850400237/0118850400237000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/23/7/0118850400237/0118850400237000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza 4 quesos envase 390 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400575___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,68 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/57/5/0118850400575/0118850400575000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/57/5/0118850400575/0118850400575000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza barbacoa envase 400 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400567___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,43 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/56/7/0118850400567/0118850400567000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/56/7/0118850400567/0118850400567000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza mediterránea envase 415 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850401250___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,51 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/25/0/0118850401250/0118850401250000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/25/0/0118850401250/0118850401250000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza de pollo envase 410 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400187___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,43 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/18/7/0118850400187/0118850400187000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/18/7/0118850400187/0118850400187000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza romana envase 415 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400898___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,85 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/89/8/0118850400898/0118850400898000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/89/8/0118850400898/0118850400898000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza de jamón serrano envase 390 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850400161___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,69 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,67 &euro;",
            "PUMPriceFormatted": "6,51 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/16/1/0118850400161/0118850400161000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/16/1/0118850400161/0118850400161000p01011.jpg"
          },
          "description": "CASA TARRADELLAS pizza de pepperoni envase 410 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614171800601": "Por la compra de 2 unidades regalo de una botella de Coca Cola clásica de 2 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614171800601": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118850400542___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,85 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/54/2/0118850400542/0118850400542000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/54/2/0118850400542/0118850400542000p01011.jpg"
          },
          "description": "CAMPOFRIO pizza de carne con salsa barbacoa envase 410 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614171800601": "Por la compra de 2 unidades regalo de una botella de Coca Cola clásica de 2 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614171800601": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118850401557___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,96 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/55/7/0118850401557/0118850401557000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/55/7/0118850401557/0118850401557000p01011.jpg"
          },
          "description": "CAMPOFRIO pizza Andaluza con jamón curado incluye con salsa de tomate estilo casero envase 345 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614171800601": "Por la compra de 2 unidades regalo de una botella de Coca Cola clásica de 2 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614171800601": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118850400989___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,67 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/98/9/0118850400989/0118850400989000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/98/9/0118850400989/0118850400989000p01011.jpg"
          },
          "description": "CAMPOFRIO pizza de jamón y queso masa fina con salsa de queso cheddar envase 360 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18614171800601": "Por la compra de 2 unidades regalo de una botella de Coca Cola clásica de 2 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18614171800601": "Ver promoción ¡pulsa aquí!"
          },
          "productId": "0110118850401540___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,67 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/54/0/0118850401540/0118850401540000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/54/0/0118850401540/0118850401540000p01011.jpg"
          },
          "description": "CAMPOFRIO pizza de bacon y cebolla con salsa carbonara envase 360 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118850401490___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,24 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "5 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/49/0/0118850401490/0118850401490000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/49/0/0118850401490/0118850401490000p01011.jpg"
          },
          "description": "FUENTETAJA PORZZIONE pizza individual de jamón y queso envase 200 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814164800001": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814164800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850401581___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,47 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "10,84 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/58/1/0118850401581/0118850401581000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/58/1/0118850401581/0118850401581000p01011.jpg"
          },
          "description": "CASA MODENA Palermo pizza fresca artesana prosciutto envase 320 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814164800001": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814164800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850401573___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,47 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "11,57 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118850401573____1__150x150.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201406/13/00118850401573____1__75x75.jpg"
          },
          "description": "CASA MODENA Trapani pizza fresca artesana 3 quesos envase 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814164800001": "2ª unidad al 70% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814164800001": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118850401599___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Pizzas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,47 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "11,57 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/59/9/0118850401599/0118850401599000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/59/9/0118850401599/0118850401599000p01011.jpg"
          },
          "description": "CASA MODENA Messina pizza artesana con frankfurt envase 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Platos preparados > Pizzas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814128800201": "Con regalo de gazpacho El Corte Ingles envase 1 l"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814128800201": ""
          },
          "productId": "0110118850203235___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de carne",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "6,11 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,11 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/03/23/5/0118850203235/0118850203235000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/03/23/5/0118850203235/0118850203235000p01011.jpg"
          },
          "description": "LA TORRALBA pollo casero asado con su jugo envase 1 kg",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Platos preparados > Platos preparados de carne"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814139800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18814139800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845201054___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de pasta",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,91 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,76 &euro;",
            "PUMPriceFormatted": "1,64 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/05/4/0118845201054/0118845201054000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/05/4/0118845201054/0118845201054000p01011.jpg"
          },
          "description": "BUITONI COMPLETISSIMO tagliatelle carbonara 1 ración bandeja 280 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814139800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18814139800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845201062___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de pasta",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,80 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,76 &euro;",
            "PUMPriceFormatted": "9,86 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/06/2/0118845201062/0118845201062000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/06/2/0118845201062/0118845201062000p01011.jpg"
          },
          "description": "BUITONI COMPLETISSIMO fusilli con verduras y prosciutto 1 ración envase 280 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814139800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814139800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845201070___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de pasta",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,76 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "9,86 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/07/0/0118845201070/0118845201070000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/07/0/0118845201070/0118845201070000p01011.jpg"
          },
          "description": "BUITONI COMPLETISSIMO risotto funghi 1 ración envase 280 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814139800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18814139800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118845201047___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de pasta",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,81 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,76 &euro;",
            "PUMPriceFormatted": "9,86 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/04/7/0118845201047/0118845201047000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/04/7/0118845201047/0118845201047000p01011.jpg"
          },
          "description": "BUITONI COMPLETISSIMO fusilli bolognese 1 ración envase 280 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Platos preparados > Platos preparados de pasta"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118033000268___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de vegetales y legumbres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "8,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/26/8/0118033000268/0118033000268000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/26/8/0118033000268/0118033000268000p01011.jpg"
          },
          "description": "CARRETILLA estilo casero pisto de verduras estuche 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118033000193___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de vegetales y legumbres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,03 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "8,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/19/3/0118033000193/0118033000193000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/19/3/0118033000193/0118033000193000p01011.jpg"
          },
          "description": "CARRETILLA estilo casero guisantes con jamón y zanahoria estuche 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118033000185___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de vegetales y legumbres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,03 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "8,33 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/18/5/0118033000185/0118033000185000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/18/5/0118033000185/0118033000185000p01011.jpg"
          },
          "description": "CARRETILLA estilo casero menestra de verduras estuche 240 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800101": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118033000680___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de vegetales y legumbres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,30 &euro;",
            "onsale": true,
            "salePriceFormatted": "2 &euro;",
            "PUMPriceFormatted": "8 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/68/0/0118033000680/0118033000680000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/68/0/0118033000680/0118033000680000p01011.jpg"
          },
          "description": "CARRETILLA revuelto de setas y hongos cultivados estuche 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118002900308___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Platos preparados de vegetales y legumbres",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,45 &euro;",
            "PUMPriceFormatted": "10,36 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/30/8/0118002900308/0118002900308000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/30/8/0118002900308/0118002900308000p01011.jpg"
          },
          "description": "IBERITOS SELECCION humus crema de garbanzos envase 140 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Platos preparados > Platos preparados de vegetales y legumbres"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18814140800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814140800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118851000440___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tortillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,08 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/44/0/0118851000440/0118851000440000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/44/0/0118851000440/0118851000440000p01011.jpg"
          },
          "description": "FUENTETAJA tortilla fresca con cebolla envase 600 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        },
        {
          "gperPromotionTextMap": {
            "18814140800101": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18814140800101": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118851000457___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tortillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,08 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,80 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/45/7/0118851000457/0118851000457000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/45/7/0118851000457/0118851000457000p01011.jpg"
          },
          "description": "FUENTETAJA tortilla fresca sin cebolla envase 600 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": [
            {
              "description": "Producto refrigerado",
              "url": "http://sgfm.elcorteingles.des/SGFM/supermarket/tematicos/es/011_refrigerado.gif"
            }
          ]
        }
      ],
      "categoria": "Platos preparados > Tortillas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118029902519___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tortillas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,39 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,38 &euro;",
            "PUMPriceFormatted": "6,10 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/02/51/9/0118029902519/0118029902519000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/02/51/9/0118029902519/0118029902519000p01011.jpg"
          },
          "description": "HELIOS Torti-ya preparado para tortilla de patata con cebolla frasco 390 g neto escurrido",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Platos preparados > Vegetales diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014150800301": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014150800301": ""
          },
          "productId": "0110118016200521___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Ketchup",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,55 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "0,36 &euro; / 100 ml."
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/52/1/0118016200521/0118016200521000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/52/1/0118016200521/0118016200521000p01011.jpg"
          },
          "description": "HELLMANN'S ketchup envase 430 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Ketchup"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800103": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800103": ""
          },
          "productId": "0110118014300117___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Mayonesa y salsa ligera",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,86 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "4,13 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/7/0118014300117/0118014300117000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/7/0118014300117/0118014300117000p01011.jpg"
          },
          "description": "HELLMANN'S mayonesa frasco 450 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800104": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800104": ""
          },
          "productId": "0110118014300380___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Mayonesa y salsa ligera",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,32 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,87 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118014300380/0118014300380000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/38/0/0118014300380/0118014300380000p01011.jpg"
          },
          "description": "HELLMANN'S mayonesa frasco 225 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800105": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800105": ""
          },
          "productId": "0110118014300570___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Mayonesa y salsa ligera",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,09 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "4,86 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/57/0/0118014300570/0118014300570000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/57/0/0118014300570/0118014300570000p01011.jpg"
          },
          "description": "HELLMANN'S mayonesa bocabajo envase 430 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118014300091___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Mayonesa y salsa ligera",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,92 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,29 &euro;",
            "PUMPriceFormatted": "2,87 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/09/1/0118014300091/0118014300091000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/09/1/0118014300091/0118014300091000p01011.jpg"
          },
          "description": "CALVE mayonesa casera frasco 450 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Mayonesa y salsa ligera"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014126170201": "Lleva 3 y paga 2"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014126170201": ""
          },
          "productId": "0110118001400086___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa bechamel",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,04 &euro;",
            "onsale": true,
            "salePriceFormatted": "1 &euro;",
            "PUMPriceFormatted": "25,64 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118001400086/0118001400086000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/08/6/0118001400086/0118001400086000p01011.jpg"
          },
          "description": "GALLINA BLANCA salsa para gratinar bechamel al horno sobre 39 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Salsa bechamel"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016600407___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,35 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,84 &euro;",
            "PUMPriceFormatted": "6,46 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/40/7/0118016600407/0118016600407000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/40/7/0118016600407/0118016600407000p01011.jpg"
          },
          "description": "J.R.SUAREZ salsa chimichurri botella 285 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800102": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800102": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118017605207___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,52 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,08 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/05/20/7/0118017605207/0118017605207000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/05/20/7/0118017605207/0118017605207000p01011.jpg"
          },
          "description": "HELLMANN'S gran salsa barbacoa bocabajo envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800102": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800102": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016600183___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,52 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,08 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/18/3/0118016600183/0118016600183000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/18/3/0118016600183/0118016600183000p01011.jpg"
          },
          "description": "HELLMANN'S gran salsa burger bocabajo envase 250 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016600381___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,96 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,59 &euro;",
            "PUMPriceFormatted": "5,08 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/38/1/0118016600381/0118016600381000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/38/1/0118016600381/0118016600381000p01011.jpg"
          },
          "description": "HUNTS salsa barbacoa miel mostaza envase 510 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016600399___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,79 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,59 &euro;",
            "PUMPriceFormatted": "5,08 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/39/9/0118016600399/0118016600399000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/39/9/0118016600399/0118016600399000p01011.jpg"
          },
          "description": "HUNTS salsa barbacoa hickory envase 510 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016600373___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,96 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,59 &euro;",
            "PUMPriceFormatted": "5,08 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/37/3/0118016600373/0118016600373000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/37/3/0118016600373/0118016600373000p01011.jpg"
          },
          "description": "HUNTS salsa barbacoa original envase 510 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016700041___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,24 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,89 &euro;",
            "PUMPriceFormatted": "10,51 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/04/1/0118016700041/0118016700041000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/04/1/0118016700041/0118016700041000p01011.jpg"
          },
          "description": "SALSAS ASTURIANAS salsa de boletus botella 275 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016800254___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,91 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,89 &euro;",
            "PUMPriceFormatted": "9,03 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/25/4/0118016800254/0118016800254000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/25/4/0118016800254/0118016800254000p01011.jpg"
          },
          "description": "SALSAS ASTURIANAS salsa de mostaza con miel botella 320 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016700538___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,91 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,89 &euro;",
            "PUMPriceFormatted": "10,40 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/53/8/0118016700538/0118016700538000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/53/8/0118016700538/0118016700538000p01011.jpg"
          },
          "description": "SALSAS ASTURIANAS salsa de queso cabrales botella 278 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016700546___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para carne y fondue",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,19 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,89 &euro;",
            "PUMPriceFormatted": "10,40 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/54/6/0118016700546/0118016700546000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/54/6/0118016700546/0118016700546000p01011.jpg"
          },
          "description": "SALSAS ASTURIANAS salsa de cinco pimientas botella 287 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Salsa para carne y fondue"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800106": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800106": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800239___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "4,42 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/23/9/0118016800239/0118016800239000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/23/9/0118016800239/0118016800239000p01011.jpg"
          },
          "description": "LIGERESA salsa yogur envase 450 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800106": "2ª unidad al 50% de descuento"
          },
          "onSale": true,
          "promotionLinksMap": {
            "18014146800106": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800262___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,01 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,99 &euro;",
            "PUMPriceFormatted": "4,42 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/26/2/0118016800262/0118016800262000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/26/2/0118016800262/0118016800262000p01011.jpg"
          },
          "description": "LIGERESA salsa César botella 450 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800106": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800106": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800221___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,99 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "4,42 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/22/1/0118016800221/0118016800221000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/22/1/0118016800221/0118016800221000p01011.jpg"
          },
          "description": "LIGERESA salsa miel y mostaza envase 450 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800107": "Lleva 2 unidades por 2,50 €"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800107": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800296___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,56 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,20 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/29/6/0118016800296/0118016800296000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/29/6/0118016800296/0118016800296000p01011.jpg"
          },
          "description": "COOSUR salsa César envase 300 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800107": "Lleva 2 unidades por 2,50 €"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800107": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800288___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,56 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,20 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/28/8/0118016800288/0118016800288000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/28/8/0118016800288/0118016800288000p01011.jpg"
          },
          "description": "COOSUR salsa yogurt envase 300 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {
            "18014146800107": "Lleva 2 unidades por 2,50 €"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800107": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016800270___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,56 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "5,20 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/27/0/0118016800270/0118016800270000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/27/0/0118016800270/0118016800270000p01011.jpg"
          },
          "description": "COOSUR salsa vinagreta balsámica envase 300 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400106___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,65 &euro;",
            "PUMPriceFormatted": "1,22 &euro; / 100 gr."
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/10/6/0120904400106/0120904400106000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/10/6/0120904400106/0120904400106000p01011.jpg"
          },
          "description": "DOÑA PEPA salsa balsámica al Pedro Ximénez botella 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110120904400114___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,65 &euro;",
            "PUMPriceFormatted": "1,22 &euro; / 100 gr."
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/11/4/0120904400114/0120904400114000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/11/4/0120904400114/0120904400114000p01011.jpg"
          },
          "description": "DOÑA PEPA salsa balsámica de vinagre de Módena botella 300 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016800320___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,70 &euro;",
            "PUMPriceFormatted": "8,15 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/32/0/0118016800320/0118016800320000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/32/0/0118016800320/0118016800320000p01011.jpg"
          },
          "description": "KEN'S salsa César envase 454 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016800338___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsa para ensaladas",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,70 &euro;",
            "PUMPriceFormatted": "8,15 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/33/8/0118016800338/0118016800338000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/33/8/0118016800338/0118016800338000p01011.jpg"
          },
          "description": "KEN'S salsa miel y mostaza envase 454 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Salsa para ensaladas"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800102": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800102": "Puedes combinar productos ¡pulsa aquí!"
          },
          "productId": "0110118016600225___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas picantes y tabasco",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,52 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "6,76 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/22/5/0118016600225/0118016600225000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/22/5/0118016600225/0118016600225000p01011.jpg"
          },
          "description": "HELLMANN'S gran salsa brava envase 225 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118017605439___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas picantes y tabasco",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,05 &euro;",
            "onsale": true,
            "salePriceFormatted": "0,99 &euro;",
            "PUMPriceFormatted": "3,30 &euro; / Litro"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/05/43/9/0118017605439/0118017605439000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/05/43/9/0118017605439/0118017605439000p01011.jpg"
          },
          "description": "COOSUR salsa brava envase 300 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Salsas picantes y tabasco"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118027500711___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas usos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "2,65 &euro;",
            "onsale": true,
            "salePriceFormatted": "2,25 &euro;",
            "PUMPriceFormatted": "16,07 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/71/1/0118027500711/0118027500711000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/71/1/0118027500711/0118027500711000p01011.jpg"
          },
          "description": "HELIOS tomate para untar con aceite de oliva virgen extra pack 2 frasco 70 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118016800312___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Salsas usos diversos",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "3,95 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,70 &euro;",
            "PUMPriceFormatted": "8,15 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/31/2/0118016800312/0118016800312000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/31/2/0118016800312/0118016800312000p01011.jpg"
          },
          "description": "KEN'S salsa ranchera envase 454 ml",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Salsas usos diversos"
    },
    {
      "productos": [
        {
          "gperPromotionTextMap": {
            "18014146800001": "2ª unidad al 50% de descuento"
          },
          "onSale": false,
          "promotionLinksMap": {
            "18014146800001": ""
          },
          "productId": "0110118043500018___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,40 &euro;",
            "onsale": false,
            "salePriceFormatted": null,
            "PUMPriceFormatted": "2,46 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/01/8/0118043500018/0118043500018000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/01/8/0118043500018/0118043500018000p01011.jpg"
          },
          "description": "HELIOS tomate frito frasco 570 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043500893___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,65 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,58 &euro;",
            "PUMPriceFormatted": "2,82 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/00/89/3/0118043500893/0118043500893000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/00/89/3/0118043500893/0118043500893000p01011.jpg"
          },
          "description": "HELIOS tomate frito estilo Mediterráneo con aceite oliva virgen extra frasco 560 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043500059___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "1,67 &euro;",
            "onsale": true,
            "salePriceFormatted": "1,35 &euro;",
            "PUMPriceFormatted": "2,37 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM//00/05/9/0118043500059/0118043500059000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM//00/05/9/0118043500059/0118043500059000p01011.jpg"
          },
          "description": "HELIOS salsa de tomate casero frasco 570 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        },
        {
          "gperPromotionTextMap": {},
          "onSale": true,
          "promotionLinksMap": {},
          "productId": "0110118043501263___",
          "giftDescriptionList": [],
          "giftLinksMap": {},
          "varietyDescription": "Tomate frito",
          "tempOfferDescription": "",
          "priceInfo": {
            "basePriceFormatted": "4,50 &euro;",
            "onsale": true,
            "salePriceFormatted": "3,75 &euro;",
            "PUMPriceFormatted": "2,68 &euro; / Kilo"
          },
          "promotionDescriptionsList": [],
          "images": {
            "largeImage": "http://sgfm.elcorteingles.es/SGFM/01/26/3/0118043501263/0118043501263000m01011.jpg",
            "smallImage": "http://sgfm.elcorteingles.es/SGFM/01/26/3/0118043501263/0118043501263000p01011.jpg"
          },
          "description": "EL CORTE INGLES tomate frito cosecha pack ahorro 4 frasco 350 g",
          "productInfo": {
            "saleType": "U",
            "sellInPopup": false,
            "quantity": "",
            "packaginUnits": 1
          },
          "features": null
        }
      ],
      "categoria": "Salsas, ketchup y tomate frito > Tomate frito"
    }
  ]
}
 