(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
  
var Transformation = require('./transformation');
module.exports = {
  tap: {
    get: function(propName) {
      return function() {
        var evil = !propName.match(/(^[a-zA-Z\-\_\.\[\]\'\"\d]*$)/);
        return (evil)? undefined : eval('this.' + propName);
      }
    },

    set: function(propName, value) {
      var fn = function() { return value }
      return function() {
        var evil = !propName.match(/(^[a-zA-Z\-\_\.\[\]\'\"\d]*$)/);
        if (!evil) {
          try {
            eval('this.' + propName + '= fn()');
          } catch(err) {}
        }
      }
    },


    equals: function(prop1, prop2) {
      return function () {
        return this[prop1] == this[prop2];
      }
    },

    notEquals: function(prop1, prop2) {
      return function () {
        return this[prop1] != this[prop2];
      }
    },

    existance: function(prop) {
      return function () {
        return 'undefined' !== this[prop];
      }
    },

    lengthOf: function(propName) {
      return function () {
        return this[propName].length;
      }
    },

    numberize: function(propName) {
      var regex = /([0-9.]*)/;
      return function () {
        var match = regex.exec(this[propName]);
        if (match) return Number(match[0]);
        else return null;
      }
    }
  },

  createTransformation: function() {
    return new Transformation();
  },

  transform: function(object, transformation, context) {
    return transformation.run(object, context);
  },

  transformCollection: function(object, transformation, context) {
    return transformation.runCollection(object, context);
  },

  clone: function(object) {
    return JSON.parse(JSON.stringify(object));
  }
}

},{"./transformation":2}],2:[function(require,module,exports){
var Transformation = function() {
  this.stack = [];
}

Transformation.aliasMethod = function(methodName, aliasedMethod) {
  Transformation.prototype[methodName] = Transformation.prototype[aliasedMethod];
}

Transformation.addMethod = function(methodName, fn) {
  Transformation.prototype[methodName] = function() {
    this.stack.push(fn.apply(this, [].slice.call(arguments)));
    return this;
  }
}

Transformation.prototype.branch = function() {
  var branch = new Transformation();
  branch.setLoopback(this);
  return branch;
}

Transformation.prototype.setLoopback = function(loopback) {
  this.loopback = loopback;
}

Transformation.prototype.getContext = function() {
  return this.context;
}

Transformation.prototype.runCollection  = function(object, context) {
  var self = this;
  return object.map(function(o) { return self.run(o, context) });
}

Transformation.prototype.run = function(source, context) {
  var object = {};

  if ('object' === typeof source) for (var i in source) object[i] = source[i];
  else object = source;

  if (this.loopback) object = this.loopback.run(object, context);
  if (context) this.context = context;
  this.stack.forEach(function(transform) {
    object = transform(object);
  });
  return object;
}

Transformation.addMethod('setContext', function(context) {
  var self = this;
  return function(object) {
    self.context = context;
    return object;
  }
})

Transformation.addMethod('unsetContext', function(context) {
  var self = this;
  return function(object) {
    self.context = null;
    return object;
  }
})

Transformation.addMethod('removeProperty', function(propName) {
  return function(object) {
    delete object[propName];
    return object;
  }
})

Transformation.addMethod('filterProperties', function(arr) {
  return function(object) {
    for (var prop in object) {
      if (!~arr.indexOf(prop)) delete object[prop];
    }
    return object;
  }
})

Transformation.addMethod('castProperty', function(propName, cast) {
  return function(object) {
    object[propName] = cast(object[propName]);
    return object;
  }
})

Transformation.addMethod('renameProperty', function(oldPropName, newPropName) {
  return function(object) {
    object[newPropName] = object[oldPropName];
    delete object[oldPropName];
    return object;
  }
})

Transformation.addMethod('setProperty', function(propName, value) {
  var self = this;
  return function(object) {
    var v = ('function' === typeof value)? value.call(object, self.getContext()) : value;
    object[propName] = ('undefined' !== typeof v)? v : object[propName];
    return object;
  }
})

Transformation.addMethod('setProperties', function(properties) {
  var self = this;
  return function(object) {
    for (var propName in properties) {
      var value = properties[propName];
      var v = ('function' === typeof value)? value.call(object, self.getContext()) : value;
      object[propName] = ('undefined' !== typeof v)? v : object[propName];
    }
    return object;
  }
})

Transformation.addMethod('cloneProperty', function(propName, targetName) {
  return function(object) {
    if (!object[propName]) return object;
    object[targetName] = JSON.parse(JSON.stringify(object[propName]));
    return object;
  }
})

Transformation.addMethod('runCommand', function(command) {
  var self = this;
  return function(object) {
    command.call(object, self.getContext());
    return object;
  }
})

Transformation.addMethod('expandAsProperty', function(propName) {
  return function(object) {
    var o = {};
    o[propName] = object;
    return o;
  }
})

Transformation.addMethod('transformProperty', function(propName, transformation, newContext) {
  var self = this;
  var transformProperty = function(property) {
    context = (newContext)? newContext : self.getContext();
    return transformation.run(property, context);
  }
  return function(object) {
    if (!object[propName]) return object;
    else object[propName] = transformProperty(object[propName]);
    return object;
  }
});

Transformation.addMethod('transformCollection', function(propName, transformation, newContext) {
  var self = this;
  var transformProperty = function(property) {
    context = (newContext)? newContext : self.getContext();
    return transformation.runCollection(property, context);
  }
  return function(object) {
    if (!object[propName]) return object;
    else object[propName] = transformProperty(object[propName]);
    return object;
  }
});


Transformation.addMethod('recursiveTransform', function(propName) {
  var self = this;
  return function(object) {
    if (!object[propName]) return object;
    else object[propName] = self.run(object[propName], self.getContext());
    return object;
  }
})


Transformation.addMethod('recursiveTransformCollection', function(propName) {
  var self = this;
  return function(object) {
    if (!object[propName]) return object;
    else object[propName] = self.runCollection(object[propName], self.getContext());
    return object;
  }
})

Transformation.addMethod('groupProperties', function(properties, propName) {
  return function(object) {
    object[propName] = {};
    properties.forEach(function(fromName) {
      object[propName][fromName] = object[fromName];
      delete object[fromName];
    })
    return object;
  }
})

Transformation.aliasMethod('assignProperty',   'setProperty');
Transformation.aliasMethod('assignProperties', 'setProperties');
module.exports = Transformation;

},{}],3:[function(require,module,exports){
adapt = require('adapt');

},{"adapt":1}]},{},[3]);
